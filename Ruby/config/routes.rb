Rails.application.routes.draw do  
  resources :points
  match '*path', {
    controller: 'points',
    action: 'options',
    constraints: { method: 'OPTIONS' },
    via: [:options]
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
